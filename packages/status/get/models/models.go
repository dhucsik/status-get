package models

type Event struct {
}

type Response struct {
	Status string `json:"status,omitempty"`
	Err    string `json:"error,omitempty"`
}
