package main

import (
	"context"
	"digital-ocean/status-get/models"
	"digital-ocean/status-get/redis"
)

func Main(ctx context.Context, event models.Event) models.Response {
	status, err := redis.GetStatus(ctx)
	if err != nil {
		return models.Response{Err: err.Error()}
	}

	return models.Response{
		Status: status,
	}
}
