package redis

import (
	"context"
	"crypto/tls"
	"fmt"
	"github.com/redis/go-redis/v9"
	"os"
)

const statusKey = "status"

func GetStatus(ctx context.Context) (string, error) {
	redisClient, err := start(ctx)
	if err != nil {
		return "", err
	}

	status, err := redisClient.Get(ctx, statusKey).Result()
	if err != nil {
		return "", err
	}

	return status, nil
}

func start(ctx context.Context) (*redis.Client, error) {
	redisUsername := os.Getenv("REDIS_USER")
	redisPassword := os.Getenv("REDIS_PASSWORD")
	redisHost := os.Getenv("REDIS_HOST")
	redisPort := os.Getenv("REDIS_PORT")

	options, err := redis.ParseURL(fmt.Sprintf("redis://%s:%s@%s:%s", redisUsername, redisPassword, redisHost, redisPort))
	if err != nil {
		return nil, err
	}

	options.TLSConfig = &tls.Config{
		InsecureSkipVerify: true,
	}

	redisClient := redis.NewClient(options)
	_, err = redisClient.Ping(ctx).Result()
	if err != nil {
		return nil, err
	}

	return redisClient, nil
}
